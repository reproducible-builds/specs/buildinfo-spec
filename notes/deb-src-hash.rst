======================
Source-hash aware dpkg
======================

WORK IN PROGRESS

This is a plan for the moderately-far future and is not intended to block
existing progress on buildinfo files that will not have the features discussed.

Motivation
==========

We would like buildinfo files to contain hashes of the source packages of the
transitive build-depends. The reasoning for this is discussed in `elsewhere
<buildinfo.html>`_.

A reasonable approach is to put this information in ``/var/lib/dpkg/status``.
Then, the program that generates .buildinfo does not need to be aware of APT or
higher-level tools. (This is currently the case for ``dpkg-buildpackage``.)

Therefore, this information must be in ``DEBIAN/control`` of each binary
package.

Therefore, when building, we must add this information to
``debian/$package/DEBIAN/control``.

How to achieve this?

Proposal A
==========

1. ``dpkg-source --before-build`` should write ``debian/source/sha256`` before
   applying patches, being the sha256 of the unsigned .dsc, calculated from the
   current source tree in the same way that ``--build`` would have done.

   a. If the corresponding .dsc exists in the parent directory and the
      sha256sum of this is different, then it should exit with an error.

   b. If the .dsc does not exist, it COULD warn the user that they should
      generate one. Everything else should still work without this, though.

2. ``dpkg-source --before-build`` should verify that the hash remains the same
   after applying patches. This should be true for most current packages, since
   ``dpkg-source --build`` is already deterministic (as of 1.18.3) if run twice
   on the same source tree, and touching upstream source files retains this
   property since they are not part of the ``.dsc`` or ``.debian.tar``.

3. ``dpkg-gencontrol`` should add the content of ``debian/source/sha256`` to
   each of ``debian/*/DEBIAN/control`` as ``Source-Sha256: xxx`` if this is
   available. If not available, it can be silent (or warn?)

4. Lintian should emit an ``ERROR`` if ``Source-Sha256`` is not present in the
   control file for any binary package. This allows users to play with running
   ``debian/rules build`` directly e.g. for debugging, but gives a very strong
   indicator to Do Things Properly.

Proposal B
==========

Proposal A is problematic because step (1) can be very expensive and we might
not want to do this every time we run ``dpkg-buildpackage -b``. Here is a
cheaper and simpler alternative:

1. ``dpkg-buildpackage`` before calling ``dpkg-source --before-build`` should
   check to see if there is a corresponding ``.dsc`` in the parent directory,
   with the same filename that ``dpkg-source -b .`` would produce.

   If it does not exist, then emit a warning: "no source package, your .debs
   won't have the Source-Hash field".

2. If it does exist, then check that it contains the same filelist (including
   metadata like sizes and timestamps, which are cheap-to-access from the
   archive) as what a call to ``dpkg-source -b .`` now *would* archive - but
   don't do the actual archiving operation, which can be expensive.

   (Unfortunately, this may not work well for version 1.0 packages, since
   calculating the filelist plus metadata requires extracting the ``diff``
   tarball and applying it to the filelist of the ``orig`` tarball. However it
   should be possible to do this only using the metadata of the decompressed
   patch, without actually applying the patch on-disk.)

   If the filelists are different, then emit a warning: "existing source
   package differs from the tree being built".

3. If no warnings were emitted, we can communicate to ``dpkg-gencontrol``
   (perhaps by an environment variable) that it can hash the relevant ``.dsc``
   and add this to ``DEBIAN/control``.

   Furthermore, if this information isn't available then ``dpkg-gencontrol``
   can output a warning, in addition to the other warnings above.

4. As per (4) in Proposal A, lintian should emit an ``ERROR`` for binary
   packages that do not contain these hashes.

Post-build
==========

With both these proposals, in order for a second build to work correctly,
``debian/rules clean`` must work "perfectly", to restore the original tree
after a build. Sometimes people are a bit slack in making this work, it is not
a policy requirement, and some packaging tools like ``dh_autoreconf_clean``
completely contradict this requirement. [1] However if it doesn't work
properly, both proposals will detect this scenario, prompting the developer to
re-build the ``.dsc``, or re-extract it over the current directory to restore
the original timestamps.

Also, ``--after-build`` must completely undo the effects of ``--before-build``.
This is the case for Debian packages, which reset timestamps when applying
patches, so that these don't need to be explicitly reverted when unapplying
them. If the developer manually uses quilt, this *will* change the timestamps,
but this would also be detected by both proposals and a warning emitted.

Finally, tools like sbuild/pbuilder never try to do a second build after the
first one anyway, so this scenario will rarely be a problem in practise.

[1] At least, ``dpkg-buildpackage`` supports this requirement, and does not
itself irreversibly touch source files. (At a previous version 1.18.3, ``-S``
touched ``debian/`` for some reason, but this appears now to be fixed in the
latest versions.)
